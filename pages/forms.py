from django import forms
from django.forms.widgets import HiddenInput, Textarea, TextInput
from django.utils.translation import ugettext_lazy as _
from django.forms.widgets import RadioSelect

RATING_CHOICES = (
    ('Poor', 'Poor'),
    ('Good', 'Good'),
    ('Excellent', 'Excellent'),
)

class ContactForm(forms.Form):
    name = forms.CharField(required=False, label=_("Name"), initial=_("Name"))
    email = forms.EmailField(label=_("Email"), initial=_("Email"), help_text="Required.", error_messages={"required":"Please enter your email address.", "invalid":"Please enter a valid email address."})
    phone = forms.CharField(required=False, label=_("Phone"), initial=_("Phone"),)
    message = forms.CharField(widget=Textarea(), label=_("Message"), initial=_("Message"), help_text="Required.", error_messages={"required":"Please enter your message."})

    mnbvcxz = forms.CharField(widget=HiddenInput, required=False)
    mnbvcxz.widget.attrs['class'] = 'valid'
    
class FeedbackForm(forms.Form):
    name = forms.CharField()
    your_email = forms.EmailField(error_messages={"required":"Please enter your email address.", "invalid":"Please enter a valid email address."})
    date_of_stay = forms.DateField()
    room_occupied = forms.CharField(required=False,)
    
    quality_of_cuisine = forms.ChoiceField(choices=RATING_CHOICES, widget=RadioSelect, required=False,)
    welcome_from_staff = forms.ChoiceField(choices=RATING_CHOICES, widget=RadioSelect, required=False,) 
    staff_efficiency = forms.ChoiceField(choices=RATING_CHOICES, widget=RadioSelect, required=False,)
    room_presentation = forms.ChoiceField(choices=RATING_CHOICES, widget=RadioSelect, required=False,)
    room_cleanliness = forms.ChoiceField(choices=RATING_CHOICES, widget=RadioSelect, required=False,)
    range_of_activities = forms.ChoiceField(choices=RATING_CHOICES, widget=RadioSelect, required=False,)
    soysambu_conservancy_as_a_venue = forms.ChoiceField(choices=RATING_CHOICES, widget=RadioSelect, required=False,)
    value_for_money = forms.ChoiceField(choices=RATING_CHOICES, widget=RadioSelect, required=False,)
    
    comments = forms.CharField(widget=Textarea(), required=False,)

    mnbvcxz = forms.CharField(widget=HiddenInput, required=False)
    mnbvcxz.widget.attrs['class'] = 'valid'
    
class NewsletterForm(forms.Form):
    email = forms.EmailField(label=_("Sign up for our Newsletter"), initial=_("Your Email"), error_messages={"required":"Please enter your email address.", "invalid":"Please enter a valid email address."})

