from django.shortcuts import get_object_or_404

from pages.forms import *
from pages.models import *
from lamusoftware.generic.models import Setting

def generic(request):
    context = {
        'settings':dict(Setting.objects.values_list('name', 'value')),
        'footer_menu':Page.objects.filter(footer_menu=True, visible=True,parent=None),
        'top_menu':Page.objects.filter(top_menu=True, visible=True, parent=None),
    }
    return context



