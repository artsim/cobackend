from django import template
from django.template.defaultfilters import stringfilter
from django.contrib.sites.models import Site
from django.template.loader import get_template
from django.template import Context
from django.utils.translation import gettext_lazy as _
register = template.Library()

@register.filter
@stringfilter
def split(value, arg):
    return value.split(arg)

@register.filter
@stringfilter
def strip(value, arg):
    return value.strip(arg)
    
@register.filter
def sub(value, arg):
    try:
        return int(value) - int(arg)
    except:
        return value
        
@register.filter
def nonumber(value):
    try:
        number = int(value)
        return ""
    except ValueError:
        return value
    
    

