from pages.models import *
from django.contrib import admin

class PageAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['name', 'title', 'header', 'description', 'content', 'parent', ]}),
        ('Meta', {'fields': ['sortorder', 'visible', 'top_menu', 'footer_menu', 'homepage', ], 'classes': ['collapse',]}),
    ]
    list_display = ['name', 'parent', 'sortorder', 'top_menu', 'footer_menu', 'visible', 'homepage']
    list_editable = ['sortorder', 'visible', 'top_menu', 'footer_menu',]
    ordering = ['sortorder', 'name', ]

admin.site.register(Page, PageAdmin)
