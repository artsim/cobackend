from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from quiz import views

#  Quiz API urls
urlpatterns = [
    url(r'^(?P<qid>[0-9]+)/$', views.QuizDetails.as_view()),
    url(r'^question/(?P<fbid>\d+)/(?P<position>[0-9]+)/$', views.QuizQuestion.as_view()),
    url(r'^answer/(?P<fbid>\d+)/(?P<answer_code>[a-zA-Z0-9_.-]+)/$', 'quiz.views.submit_answer'),
    url(r'^start/(?P<fbid>\d+)/$', 'quiz.views.StartQuiz'),
    url(r'^restart/(?P<fbid>\d+)/$', 'quiz.views.RestartQuiz'),
    url(r'^personality/(?P<fbid>\d+)/$', 'quiz.views.UserPersonality'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
