# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0004_auto_20170411_2252'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='quizanswers',
            options={'verbose_name': 'Quiz Answers', 'verbose_name_plural': 'Quiz Answers'},
        ),
    ]
