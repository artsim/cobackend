# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0003_auto_20170329_1058'),
    ]

    operations = [
        migrations.CreateModel(
            name='QuizAnswers',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question_id', models.IntegerField()),
                ('answer_choice', models.CharField(max_length=1, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='QuizResponse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('responder', models.CharField(unique=True, max_length=126)),
                ('started', models.BooleanField(default=False)),
                ('finished', models.BooleanField(default=False)),
                ('quiz_position', models.IntegerField(default=0)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('quiz', models.ForeignKey(to='quiz.Quiz')),
            ],
        ),
        migrations.AddField(
            model_name='quizanswers',
            name='quiz_response',
            field=models.ForeignKey(to='quiz.QuizResponse'),
        ),
    ]
