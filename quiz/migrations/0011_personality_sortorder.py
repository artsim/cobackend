# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0010_auto_20170417_1622'),
    ]

    operations = [
        migrations.AddField(
            model_name='personality',
            name='sortorder',
            field=models.IntegerField(default=0, null=True, db_index=True, blank=True),
        ),
    ]
