# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0007_auto_20170416_1428'),
    ]

    operations = [
        migrations.AlterField(
            model_name='quizanswers',
            name='quiz_response',
            field=models.ForeignKey(related_name='answers', to='quiz.QuizResponse'),
        ),
    ]
