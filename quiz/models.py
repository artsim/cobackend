from django.db import models
from taggit.managers import TaggableManager


class Quiz(models.Model):
    name = models.CharField(max_length=400)
    description = models.TextField()

    class Meta:
        verbose_name = "Quiz"
        verbose_name_plural = "Quiz"

    def __unicode__(self):
        return (self.name)

    def questions(self):
        if self.pk:
            return Question.objects.filter(survey=self.pk)
        else:
            return None

    @property
    def length(self):
        return self.questions.count


class Question(models.Model):
    text = models.TextField()
    required = models.BooleanField()
    position = models.IntegerField(blank=True, null=True)
    quiz = models.ForeignKey(Quiz, related_name="questions")

    def next(self):
        quiz = Quiz.objects.get(id=self.survey_id)
        next_questions = \
            quiz.question_set.order_by('id').filter(id__gt=self.id)
        return next_questions[0] if next_questions else None

    def __unicode__(self):
        return (self.text)


class Response(models.Model):
    text = models.TextField()
    question = models.ForeignKey(Question, related_name="responses")

    def __unicode__(self):
        return (self.text)


class Answer(models.Model):
    question = models.ForeignKey(Question)
    response = models.ForeignKey(Response)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return '%s' % self.response


class QuizResponse(models.Model):
    quiz = models.ForeignKey(Quiz)
    responder = models.CharField(blank=False, null=False, unique=True, max_length=128)
    started = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)
    result = models.CharField(blank=True, max_length=128)
    position = models.IntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.responder


class QuizAnswers(models.Model):
    question_id = models.IntegerField()
    answer_choice = models.CharField(blank=False, null=True, max_length=1)
    quiz_response = models.ForeignKey(QuizResponse, related_name="answers")

    class Meta:
        verbose_name = "Quiz Answers"
        verbose_name_plural = "Quiz Answers"


class Personality(models.Model):
    name = models.CharField(blank=False, null=True, max_length=128)
    description = models.TextField(blank=True)
    careers = TaggableManager(blank=True)
    sortorder = models.IntegerField(db_index=True, blank=True, null=True, default=0)

    class Meta:
        verbose_name = "Personality"
        verbose_name_plural = "Personality"

    def __unicode__(self):
        return self.name

