from rest_framework import serializers
from quiz.models import Quiz, Question, Response, QuizAnswers, QuizResponse


class ResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Response
        fields = ('id', 'text')


class QuestionSerializer(serializers.ModelSerializer):
    responses = ResponseSerializer(many=True, read_only=True)

    class Meta:
        model = Question
        fields = ('text', 'position', 'responses')
        read_only_fields = ('responses',)


class QuizSerializer(serializers.ModelSerializer):
    class Meta:
        model = Quiz
        fields = ('id', 'name', 'length')


class QuizAnswersSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuizAnswers
        fields = ('question_id', 'answer_choice', 'quiz_response')


class QuizResponseSerializer(serializers.ModelSerializer):
    #answers = QuizAnswersSerializer(many=True, read_only=False)

    class Meta:
        model = QuizResponse
        fields = ('quiz', 'responder', 'started', 'finished', 'quiz_position')
