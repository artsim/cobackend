
def scoring(qanda):
    dIscore = 0;
    dEscore = 0;
    dSscore = 0;
    dNscore = 0;
    dTscore = 0;
    dFscore = 0;
    dJscore = 0;
    dPscore = 0;

    dI = {2: 'A', 6: 'A', 11: 'A', 15: 'B', 19: 'B'}
    dE = {2: 'B', 6: 'B', 11: 'B', 15: 'A', 19: 'A'}
    dS = {1: 'B', 10: 'B', 13: 'A', 16: 'A', 17: 'A'}
    dN = {1: 'A', 10: 'A', 13: 'B', 16: 'B', 17: 'B'}
    dT = {3: 'A', 5: 'A', 12: 'A', 14: 'B', 20: 'A'}
    dF = {3: 'B', 5: 'B', 12: 'B', 14: 'A', 20: 'B'}
    dJ = {4: 'A', 7: 'A', 8: 'B', 9: 'A', 18: 'B'}
    dP = {4: 'B', 7: 'B', 8: 'A', 9: 'B', 18: 'A'}

    for key in qanda:
        if key in dI:
            ans = qanda.get(key)
            val = dI.get(key)
            if ans == val:
                dIscore += 1
        elif key in dE:
            ans = qanda.get(key)
            val = dE.get(key)
            if ans == val:
                dEscore += 1
        elif key in dS:
            ans = qanda.get(key)
            val = dS.get(key)
            if ans == val:
                dSscore += 1
        elif key in dN:
            ans = qanda.get(key)
            val = dN.get(key)
            if ans == val:
                dNscore += 1
        elif key in dT:
            ans = qanda.get(key)
            val = dT.get(key)
            if ans == val:
                dTscore += 1
        elif key in dF:
            ans = qanda.get(key)
            val = dF.get(key)
            if ans == val:
                dFscore += 1
        elif key in dJ:
            ans = qanda.get(key)
            val = dJ.get(key)
            if ans == val:
                dJscore += 1
        elif key in dP:
            ans = qanda.get(key)
            val = dP.get(key)
            if ans == val:
                dPscore += 1

    classification = ""

    if dIscore > dEscore:
        classification += "I"
    else:
        classification += "E"

    if dSscore > dNscore:
        classification += "S"
    else:
        classification += "N"

    if dTscore > dFscore:
        classification += "T"
    else:
        classification += "F"

    if dJscore > dPscore:
        classification += "J"
    else:
        classification += "P"

    return classification
