from resumeparser.models import Resume, Industry, Keyword

import os
import PyPDF2
import csv
import textract

from docopt import docopt
from os import sys, path


from docx import Document
from docx.document import Document as _Document
from docx.oxml.text.paragraph import CT_P
from docx.oxml.table import CT_Tbl
from docx.table import _Cell, Table
from docx.text.paragraph import Paragraph


def parse_word_doc(resume):
    """
    Open a word document filetype and parse contents to string variable
    for matching comparison.
    """


    def iter_block_items(parent):
        """
        Generate a reference to each paragraph and table child within *parent*,
        in document order. Each returned value is an instance of either Table or
        Paragraph. *parent* would most commonly be a reference to a main
        Document object, but also works for a _Cell object, which itself can
        contain paragraphs and tables.
        """
        if isinstance(parent, _Document):
            parent_elm = parent.element.body
        elif isinstance(parent, _Cell):
            parent_elm = parent._tc

        for child in parent_elm.iterchildren():
            if isinstance(child, CT_P):
                yield Paragraph(child, parent)
            elif isinstance(child, CT_Tbl):
                yield Table(child, parent)

    # create empty string variable for storing file content
    docText = ''
    # set the document object with the file
    document = Document(resume)

    # iterate over blocks in the document object
    for block in iter_block_items(document):
        # if block type is paragraph, simply grab text from paragraph
        if isinstance(block, Paragraph):
            # append block text to text variable
            docText += block.text

        # if block type is table, we must iterate over the table components to get
        # content out of the cells
        elif isinstance(block, Table):
            # iterate over the rows inside the table
            for row in block.rows:
                # iterate over each cell inside each row
                for cell in row.cells:
                    # append cell text to text variable
                    docText += cell.text

    return docText.strip() or None


def parse_doc(resume):
    """
    Open a pdf document filetype and parse contents to string variable
    for matching comparison.
    """
    docText = ''
    # open the file, with read/binary priviledges
    #f = open(resume, 'rb')
    #pdf = PyPDF2.PdfFileReader(f)
    #for page in pdf.pages :
    #    docText += page.extractText()

    #f.close()
    docText = textract.process(resume)
    return docText.strip() or None


def get_rank(text, keywords):
    """
    Get the rank of the file based on total count of keywords found in the file
    contents.
    """
    # set the initial rank and count to 0
    rank = count = 0
    reload(sys)
    sys.setdefaultencoding('utf8')

    # get the percentage that each keyword is worth
    word_percentage = round(float(100)/float(len(keywords)), 2)
    # iterate over list of keywords
    for keyword in keywords:
        keyword_name = keyword.name
        multiplier = keyword.multiplier

        # was the keyword found in the file? increase overall percentage if true
        try:
            #rank += word_percentage if keyword_name.upper() in text.upper() else 0
            if keyword_name in text:
                print keyword_name
                print multiplier
                rank += word_percentage * int(multiplier)
                count += 1
            else:
                rank += 0
            #count += text.upper().count(keyword_name.upper())
        except Exception as e:
            print e

        # get the number of occurrences of the keyword in the file

    return (rank, count)


def Rank(owner):
    """
    Check the file extension against known/valid extensions and call
    associated parsing method appropriately.
    """
    resume = Resume.objects.get(owner=owner)
    # get industry related keywords_list
    industry = resume.industry
    # initalize blank keywords objects list
    keywords_list = []

    for keyword in industry.keywords.all():
        keywords_list.append(keyword)

    cvfile = resume.datafile.path
    # get the file extension from the filename
    # extension = os.path.splitext(cvfile)[1]
    # if the file passed in is a valid file
    if os.path.isfile(cvfile):
        # figure out extension to determine what parsing methodology to use
        # if extension == '.docx':
        text = parse_doc(cvfile)
        # elif extension == '.pdf':
        #    text = parse_pdf_doc(cvfile)

        if text:
            rank, count = get_rank(text, keywords_list)

        usercv = Resume.objects.get(owner=resume.owner)
        usercv.industry = industry
        usercv.rank = rank
        usercv.keywords_count = count
        usercv.processing = "Completed"
        usercv.save()
