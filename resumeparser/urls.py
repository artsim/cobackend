from django.conf.urls import include, url
from resumeparser import views


urlpatterns = [
    url(r'^resumes/', views.ResumeViewSet.as_view({'get': 'list', 'post': 'create'})),
    url(r'^resume/(?P<fbid>\d+)/$', 'resumeparser.views.ResumeView'),
    url(r'^review/(?P<fbid>\d+)/(?P<industry>[a-zA-Z0-9_.-]+)/?$', 'resumeparser.views.review'),
]
