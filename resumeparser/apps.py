from django.apps import AppConfig


class ApiConfig(AppConfig):
    name = 'Resume'
    verbose_name = "Resumes"
