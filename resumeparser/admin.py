from django.contrib import admin
from resumeparser.models import Resume, Keyword, Industry

import nested_admin


class ResumeAdmin(admin.ModelAdmin):
    model = Resume


class KeywordAdmin(nested_admin.NestedTabularInline):
    model = Keyword


class IndustryAdmin(admin.ModelAdmin):
    model = Industry
    inlines = [KeywordAdmin, ]


admin.site.register(Resume, ResumeAdmin)
admin.site.register(Industry, IndustryAdmin)

