from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from resumeparser.utils import validator
from django_q.tasks import async, result
from taggit.managers import TaggableManager


class Industry(models.Model):
    name = models.CharField(blank=False, max_length=128)
    sortorder = models.IntegerField(db_index=True, blank=True, null=True, default=0)

    class Meta:
        verbose_name = "Industry"
        verbose_name_plural = "Industries"

    def __str__(self):
        return self.name


class Keyword(models.Model):
    name = models.CharField(blank=False, max_length=128)
    multiplier = models.IntegerField(blank=True, null=True, default=0)
    industry = models.ForeignKey(Industry, related_name='keywords')

    class Meta:
        verbose_name = "Keyword"
        verbose_name_plural = "Keywords"

    def __str__(self):
        return self.name


class Resume(models.Model):
    owner = models.CharField(max_length=70)
    uploaded = models.DateTimeField(auto_now_add=True)
    rank = models.FloatField(blank=True, default=0, null=True)
    keywords_count = models.FloatField(blank=True, default=0, null=True)
    industry = models.ForeignKey(Industry, blank=True, null=True)
    datafile = models.FileField(upload_to='uploads/resumes/', validators=[validator.validate_file_extension])
    processing = models.CharField(max_length=70, blank=True)


    def __str__(self):
        return self.owner


#@receiver(post_save, sender=Resume, dispatch_uid="update_resume_rank")
#def update_rank(sender, instance, **kwargs):
#    print "Post save file processing"
#    owner = instance.owner
#    task_id = async(
#        'resumeparser.tasks.rank', owner=owner
#    )
#    ret = result(task_id)
#    print ret
