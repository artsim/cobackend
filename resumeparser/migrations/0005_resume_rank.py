# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('resumeparser', '0004_auto_20170428_1747'),
    ]

    operations = [
        migrations.AddField(
            model_name='resume',
            name='rank',
            field=models.FloatField(default=0, null=True, blank=True),
        ),
    ]
