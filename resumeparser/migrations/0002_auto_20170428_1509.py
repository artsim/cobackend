# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('resumeparser', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resume',
            name='file_id',
            field=models.ForeignKey(to='resumeparser.ResumeArchive'),
        ),
    ]
