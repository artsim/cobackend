# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('resumeparser', '0009_auto_20170502_2247'),
    ]

    operations = [
        migrations.AddField(
            model_name='resume',
            name='keywords_count',
            field=models.FloatField(default=0, null=True, blank=True),
        ),
    ]
