# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('resumeparser', '0008_industry'),
    ]

    operations = [
        migrations.CreateModel(
            name='Keyword',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128)),
                ('multiplier', models.IntegerField(default=0, null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Keyword',
                'verbose_name_plural': 'Keywords',
            },
        ),
        migrations.AlterModelOptions(
            name='industry',
            options={'verbose_name': 'Industry', 'verbose_name_plural': 'Industries'},
        ),
        migrations.RemoveField(
            model_name='industry',
            name='keywords',
        ),
        migrations.AddField(
            model_name='keyword',
            name='industry',
            field=models.ForeignKey(related_name='keywords', to='resumeparser.Industry'),
        ),
    ]
