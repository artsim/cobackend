# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('resumeparser', '0012_resume_processing'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resume',
            name='industry',
            field=models.ForeignKey(blank=True, to='resumeparser.Industry', null=True),
        ),
        migrations.AlterField(
            model_name='resume',
            name='processing',
            field=models.CharField(max_length=70, blank=True),
        ),
    ]
