# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import resumeparser.utils.validator


class Migration(migrations.Migration):

    dependencies = [
        ('resumeparser', '0006_auto_20170429_2227'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resume',
            name='datafile',
            field=models.FileField(upload_to=b'uploads/resumes/', validators=[resumeparser.utils.validator.validate_file_extension]),
        ),
    ]
