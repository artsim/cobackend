# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('resumeparser', '0010_resume_keywords_count'),
    ]

    operations = [
        migrations.AddField(
            model_name='resume',
            name='industry',
            field=models.ForeignKey(default=1, blank=True, to='resumeparser.Industry'),
            preserve_default=False,
        ),
    ]
