# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import resumeparser.utils.validator


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Resume',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('owner', models.CharField(max_length=70)),
            ],
        ),
        migrations.CreateModel(
            name='ResumeArchive',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uploaded', models.DateTimeField(auto_now_add=True)),
                ('datafile', models.FileField(upload_to=b'uploads/resumes/%Y/%m/%d', validators=[resumeparser.utils.validator.validate_file_extension])),
            ],
        ),
        migrations.AddField(
            model_name='resume',
            name='file_id',
            field=models.ForeignKey(default=0, to='resumeparser.ResumeArchive'),
        ),
    ]
