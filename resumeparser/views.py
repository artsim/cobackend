from rest_framework import authentication, permissions, status
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response

from .models import Resume, Industry
from .serializers import ResumeSerializer

from django.http import JsonResponse
from resumeparser.utils import validator
from django_q.tasks import async, result

class DefaultsMixin(object):
    """ Default settings for view authentication, permissions, filtering and pagination. """

    authentication_classes = (
        authentication.BasicAuthentication,
        authentication.TokenAuthentication,
    )
    permission_classes = (
        permissions.IsAuthenticated,
    )


class ResumeViewSet(DefaultsMixin, ModelViewSet):

    queryset = Resume.objects.all()
    parser_classes = (MultiPartParser, FormParser, )


    def perform_create(self):
        uploaded_file = self.request.data.get('datafile')
        owner = self.request.data.get('fbid')
        resume = {
            'owner': owner,
            'datafile': uploaded_file,
        }
        resume_serializer = ResumeSerializer(data=resume)

        if resume_serializer.is_valid():
            resume_serializer.save()

        return resume_serializer

    def create(self, request, *args, **kwargs):
        resume_serializer = self.perform_create()
        headers = self.get_success_headers(resume_serializer.data)
        return Response(resume_serializer.data, status=status.HTTP_201_CREATED, headers=headers)

def ResumeView(request, fbid):
    resume = Resume.objects.get(owner=fbid)
    data = ResumeSerializer(resume).data
    return JsonResponse(data)


def review(request, fbid, industry):
    if industry == "Admin":
        industry_name = "Administration"
    elif industry == "Sales":
        industry_name = "Sales & Retail"
    elif industry == "IT":
        industry_name = "IT"

    resume = Resume.objects.get(owner=fbid)
    industry = Industry.objects.get(name=industry_name)
    resume.industry = industry
    resume.processing = "Started"
    resume.save()

    task_id = async(
        'resumeparser.tasks.rank', owner=fbid
    )
    return JsonResponse({"task": task_id, "processing": "Started"})
