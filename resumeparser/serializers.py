from rest_framework import serializers
from rest_framework.reverse import reverse

from .models import Resume


class ResumeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Resume
        fields = ('id', 'owner', 'datafile', 'uploaded', 'rank', 'keywords_count', 'processing' )
