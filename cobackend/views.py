from django.shortcuts import render_to_response
from django.template import TemplateDoesNotExist, RequestContext, loader

def privacy(request):
    template_name = 'privacy.html'
    try:
        loader.get_template(template_name)
    except TemplateDoesNotExist:
        template_name = 'page.html'

    return render_to_response(template_name )
