from django.conf.urls import include, url
from django.contrib import admin
from .views import privacy

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^nested_admin/', include('nested_admin.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^quiz/', include('quiz.urls')),
    url(r'^api/', include('resumeparser.urls')),
    url(r'^privacy/', privacy),
]
